# Translation of Development (trunk) in Portuguese (Portugal)
# This file is distributed under the same license as the Development (trunk) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2014-10-08 17:34:15+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: GlotPress/1.0-alpha-1000\n"
"Project-Id-Version: Development (trunk)\n"

#: wordpress-importer.php:1017
msgid "Choose a WXR (.xml) file to upload, then click Upload file and import."
msgstr "Escolha um ficheiro WXR (.xml) para carregar, depois clique em \"Enviar ficheiro e importar\"."

#: wordpress-importer.php:889
msgid "Zero size file downloaded"
msgstr "O ficheiro descarregado está vazio"

#: wordpress-importer.php:877
msgid "Remote server returned error response %1$d %2$s"
msgstr "O servidor remoto devolveu a resposta de erro %1$d %2$s"

#: wordpress-importer.php:235
msgid "Failed to import author %s. Their posts will be attributed to the current user."
msgstr "A importação do autor %s falhou. Os seus artigos serão atribuídos ao utilizador actual."

#: wordpress-importer.php:262
msgid "To make it easier for you to edit and save the imported content, you may want to reassign the author of the imported item to an existing user of this site. For example, you may want to import all the entries as <code>admin</code>s entries."
msgstr "Para facilitar a edição e gravação do conteúdo importado, poderá querer alterar o autor desse conteúdo para um utilizador existente neste site. Por exemplo, poderá querer importar todo o conteúdo como entradas do utilizador <code>admin</code>."

#: wordpress-importer.php:522
msgid "Failed to import &#8220;%s&#8221;: Invalid post type %s"
msgstr "Falhou a importação de &#8220;%s&#8221;: %s é um tipo de conteúdo inválido"

#: wordpress-importer.php:500 wordpress-importer.php:626
msgid "Failed to import %s %s"
msgstr "Falhou a importação de %s %s"

#: wordpress-importer.php:418
msgid "Failed to import category %s"
msgstr "Falhou a importação da categoria %s"

#: wordpress-importer.php:456
msgid "Failed to import post tag %s"
msgstr "Falhou a importação da etiqueta %s"

#: wordpress-importer.php:543
msgid "%s &#8220;%s&#8221; already exists."
msgstr "%s &#8220;%s&#8221; já existe."

#: wordpress-importer.php:744
msgid "Menu item skipped due to missing menu slug"
msgstr "Item do menu ignorado pois não contém URL"

#: wordpress-importer.php:598
msgid "Failed to import %s &#8220;%s&#8221;"
msgstr "Falhou a importação de %s &#8220;%s&#8221;"

#: wordpress-importer.php:316
msgid "assign posts to an existing user:"
msgstr "atribuir artigos a um utilizador existente:"

#: wordpress-importer.php:308
msgid "as a new user:"
msgstr "como um novo utilizador:"

#: wordpress-importer.php:210
msgid "This WXR file (version %s) may not be supported by this version of the importer. Please consider updating."
msgstr "Este ficheiro WXR (versão %s) pode não ser suportado por esta versão do importador. Considere instalar uma actualização mais recente."

#: wordpress-importer.php:264
msgid "If a new user is created by WordPress, a new password will be randomly generated and the new user&#8217;s role will be set as %s. Manually changing the new user&#8217;s details will be necessary."
msgstr "Se um novo utilizador for criado pelo WordPress, será gerada uma nova senha de forma aleatória e a sua função definido como sendo %s. Não será necessário alterar manualmente os detalhes do novo utilizador."

#: wordpress-importer.php:751
msgid "Menu item skipped due to invalid menu slug: %s"
msgstr "Item do menu ignorado pois seu URL é inválido: %s"

#: wordpress-importer.php:814
msgid "Fetching attachments is not enabled"
msgstr "O carregamento de anexos não está activado"

#: wordpress-importer.php:179
msgid "Remember to update the passwords and roles of imported users."
msgstr "Lembre-se the actualizar as senhas e funções do utilizadores importados."

#: wordpress-importer.php:135
msgid "The file does not exist, please try again."
msgstr "O ficheiro não existe, tente outra vez."

#: parsers.php:67 parsers.php:72 parsers.php:262 parsers.php:451
msgid "This does not appear to be a WXR file, missing/invalid WXR version number"
msgstr "Isto não parece ser um ficheiro WXR válido. O número de versão WXR é inválido ou não está presente."

#: parsers.php:43
msgid "Details are shown above. The importer will now try again with a different parser..."
msgstr "Os detalhes são mostrados acima. O importador irá agora tentar de novo utilizando uma análise diferente..."

#. Description of the plugin/theme
msgid "Import posts, pages, comments, custom fields, categories, tags and more from a WordPress export file."
msgstr "Importar artigos, páginas, comentários, campos personalizados, categorias, etiquetas e mais a partir de um ficheiro de exportação WordPress."

#: wordpress-importer.php:1001
msgid "A new version of this importer is available. Please update to version %s to ensure compatibility with newer export files."
msgstr "Está disponível uma nova versão do importador. Actualize para a versão %s para garantir a compatibilidade com os ficheiros de exportação mais recentes."

#: parsers.php:42 parsers.php:63
msgid "There was an error when reading this WXR file"
msgstr "Ocorreu um erro ao ler este ficheiro WXR"

#: wordpress-importer.php:305
msgid "or create new user with login name:"
msgstr "ou criar um novo nome de utilizador:"

#: wordpress-importer.php:369
msgid "Failed to create new user for %s. Their posts will be attributed to the current user."
msgstr "A criação do novo utilizador %s falhou. Os seus artigos serão atribuídos ao utilizador actual."

#: wordpress-importer.php:318
msgid "or assign posts to an existing user:"
msgstr "ou atribuir artigos a um utilizador existente:"

#: wordpress-importer.php:884
msgid "Remote file is incorrect size"
msgstr "Tamanho incorrecto do ficheiro remoto"

#. Author URI of the plugin/theme
msgid "http://wordpress.org/"
msgstr "http://wordpress.org/"

#: wordpress-importer.php:277
msgid "Download and import file attachments"
msgstr "Descarregar e importar anexos"

#. Plugin Name of the plugin/theme
msgid "WordPress Importer"
msgstr "Importador do WordPress"

#: wordpress-importer.php:281
msgid "Submit"
msgstr "Submeter"

#: wordpress-importer.php:319
msgid "- Select -"
msgstr "- Seleccionar -"

#: wordpress-importer.php:895
msgid "Remote file is too large, limit is %s"
msgstr "O ficheiro remoto é demasiado grande, o limite é %s"

#: wordpress-importer.php:178
msgid "All done."
msgstr "Concluído."

#: wordpress-importer.php:178
msgid "Have fun!"
msgstr "Divirta-se!"

#: wordpress-importer.php:827
msgid "Invalid file type"
msgstr "Tipo de ficheiro inválido"

#: wordpress-importer.php:294
msgid "Import author:"
msgstr "Importar autor:"

#: wordpress-importer.php:274
msgid "Import Attachments"
msgstr "Importar anexos"

#: wordpress-importer.php:871
msgid "Remote server did not respond"
msgstr "O servidor remoto não respondeu"

#: wordpress-importer.php:1091
msgid "Import <strong>posts, pages, comments, custom fields, categories, and tags</strong> from a WordPress export file."
msgstr "Importar <strong>artigos, páginas, comentários, campos personalizados e categorias</strong> de um ficheiro de exportação WordPress."

#: wordpress-importer.php:134 wordpress-importer.php:143
#: wordpress-importer.php:194 wordpress-importer.php:202
msgid "Sorry, there has been an error."
msgstr "Ocorreu um erro."

#. Plugin URI of the plugin/theme
msgid "http://wordpress.org/extend/plugins/wordpress-importer/"
msgstr "http://wordpress.org/extend/plugins/wordpress-importer/"

#. Author of the plugin/theme
msgid "wordpressdotorg"
msgstr "wordpressdotorg"

#: wordpress-importer.php:261
msgid "Assign Authors"
msgstr "Atribuir autores"

#: wordpress-importer.php:1016
msgid "Howdy! Upload your WordPress eXtended RSS (WXR) file and we&#8217;ll import the posts, pages, comments, custom fields, categories, and tags into this site."
msgstr "Olá! Carregue o seu ficheiro WordPress eXtended RSS (WXR) e importaremos os artigos, páginas, comentários, campos personalizados, categorias e etiquetas para este site."

#: wordpress-importer.php:994
msgid "Import WordPress"
msgstr "Importar de WordPress"